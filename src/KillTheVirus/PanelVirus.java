package KillTheVirus;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

public class PanelVirus extends JPanel implements MouseListener  {
    private ArrayList<Virus> virus = new ArrayList<Virus>();
    private int x,y;

    public void addVirus(){
        Virus v = new Virus();

        v.setElPanelComu(this);

        virus.add(v);

        v.start();
    }
    public PanelVirus(){
        addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        x = mouseEvent.getX();
        y = mouseEvent.getY();
        //printa per consola les coordenades del mouse en format (x,y)
        System.out.println("(" + x + "," + y + ")");
        repaint(); //es crida a paint()
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    public void paint (Graphics graphics){
        super.paint(graphics);
        Graphics2D g = (Graphics2D) graphics;
        for (Virus v : virus){
            graphics.drawImage(v.getImage(),x,y,null );
        }
        Toolkit.getDefaultToolkit().sync();
    }

}
