package KillTheVirus;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.net.URL;

public class Virus extends Thread{
    private int coordx;
    private int coordy;
    private double dx = 1;
    private double dy = 1;
    private int radi = 15;
    private Image image;
    private boolean isAlive;

    URL url = getClass().getClassLoader().getResource("KillTheVirus/imatges/virus.png");

    public Image getImage() {
        return image = new ImageIcon(url).getImage();
    }

    private JPanel elPanelComu; //el panell on reboten el virus

    public void setElPanelComu(JPanel p) {
        elPanelComu = p;
    }

    public void moureVirus(){
        Rectangle2D limits = elPanelComu.getBounds();
        double width = limits.getWidth();
        double height = limits.getHeight();
        coordx += dx;
        coordy += dy;
        if (coordx + radi / 2 > width || coordx + radi / 2 < 0) dx = -dx;
        if (coordy + radi / 2 > height || coordy + radi / 2 < 0) dy = -dy;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 2000; i++) {
            moureVirus();
            try {
                Thread.sleep(4);
            } catch (Exception e) {
            }
            elPanelComu.repaint();
        }
    }
}
