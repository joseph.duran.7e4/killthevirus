package KillTheVirus;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KillTheVirusGUI extends JFrame {
    private PanelVirus panelVirus = new PanelVirus();

    public KillTheVirusGUI() {
        setBounds(600, 300, 400, 350);
        setTitle("Virus Botant");
        add(panelVirus, BorderLayout.CENTER);
        JPanel botonera = new JPanel();
        JButton boto1 = new JButton("Virus va!");
        JButton boto2 = new JButton("Sortir");
        botonera.add(boto1);
        botonera.add(boto2);
        boto1.addActionListener(new ClickBotoPilotaVa());
        boto2.addActionListener(new ClickSortir());
        add(botonera, BorderLayout.SOUTH);
    }

    class ClickSortir implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.exit(0);
        }
    }

    class ClickBotoPilotaVa implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            panelVirus.addVirus();
        }
    }
}
